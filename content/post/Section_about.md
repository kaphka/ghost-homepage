---
title: About Ghost
comments: false
bigimg: [{src: "/img/triangle.jpg", desc: "Triangle"}, {src: "/img/sphere.jpg", desc: "Sphere"}, {src: "/img/hexagon.jpg", desc: "Hexagon"}]
---

With our non–invasive smart glove and training–app solution we want to empower patients who lost the feeling in their hands, reducing phantom pain and follow up injuries as well as reducing the constant financial burden on the health insurances, relating to the nerve damage.

*At this point we are not a founded company just yet but are in the process.
